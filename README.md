**Symfony 4 - Currency Tracker**

This is a web application for tracking European Central Bank and Central Bank of the Russian Federation currency rates.
Besides this, you can calculate currency rates based on ECB and CBR rates.

---

## Installation

First, you need to clone this repository to your local machine:

```git clone https://ferganze@bitbucket.org/ferganze/symfony-4-currency-tracker.git```


Then, run the command:

```composer install```


After everything installed successfully, you have to set up your DB configuration in **.env** file:

```DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name```


If you have not created database yet, you can run this command:

```php bin/console doctrine:schema:create```

And lastly, upgrade your database in order to have required tables:

```php bin/console doctrine:schema:update --force```

---

## .ENV Configuration

* Change your application URL - **APP_URL**.
* You can choose either **ECB** or **CBR** for **RATE_SOURCE** in order to calculate your currency rates.

---

## Run Currency Tracker

You can update Currency rates in your Database via running Doctrine Fixtures:

```php bin/console doctrine:fixtures:load```

---

## REST API

Before get your REST API worked, you need to run the web application:

```composer require server --dev```

```php bin/console server:run```


Then you can send a **POST** request to the URL **{APP_URL}/api/calculate/{BASE_CURRENCY}-{QUOTED_CURRENCY}/{AMOUNT}** in order to get actual currency rates. Like:

```curl -X POST http://127.0.0.1:8000/api/calculate/GBP-USD/100```

---

## Testing

For testing the application, please run the command:

```php bin/phpunit```