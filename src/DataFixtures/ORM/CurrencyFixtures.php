<?php

namespace App\DataFixtures\ORM;

use App\Entity\ECB;
use App\Entity\CBR;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CurrencyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /**
         * Fetch Currency rates from European Central Bank
         */
        $ecb_data = simplexml_load_string(file_get_contents('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'));
        foreach ($ecb_data->Cube->Cube->Cube as $currency) {
            $ecb = new ECB();
            $ecb->setCurrency($currency['currency']);
            $ecb->setRate($currency['rate']);
            $manager->persist($ecb);
        }

        /**
         * Fetch Currency rates from Central Bank of the Russian Federation
         */
        $cbr_data = simplexml_load_string(file_get_contents('https://www.cbr.ru/scripts/XML_daily.asp'));
        foreach ($cbr_data->Valute as $currency) {
            $cbr = new CBR();
            $cbr->setCurrency($currency->CharCode);
            $cbr->setRate(number_format($currency->Nominal / $currency->Value, 4));
            $manager->persist($cbr);
        }

        $manager->flush();
    }
}