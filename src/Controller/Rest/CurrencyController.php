<?php

namespace App\Controller\Rest;

use App\Entity\ECB;
use App\Entity\CBR;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class CurrencyController extends AbstractFOSRestController
{
    /**
     * Calculates currency rates
     * @Rest\Post(
     *      "/calculate/{baseCurrency}-{quotedCurrency}/{amount}",
     *      name="calculate",
     *      requirements={
     *          "baseCurrency": "\w+",
     *          "quotedCurrency": "\w+",
     *          "amount": "\d+"
     *      }
     * )
     * @return View
     */
    public function calculate($baseCurrency, $quotedCurrency, $amount): View
    {
        if (getenv('RATE_SOURCE') == 'ECB') {
            $repository = $this->getDoctrine()->getRepository(ECB::class);
        } else {
            $repository = $this->getDoctrine()->getRepository(CBR::class);
        }

        $base   = $repository->findOneBy(['currency' => $baseCurrency]);
        $quoted = $repository->findOneBy(['currency' => $quotedCurrency]);

        if (!empty($base) && !empty($quoted)) {
            $rate   = $quoted->getRate() / $base->getRate();
            $result = number_format($amount * $rate, 2);
            return View::create(['result' => $result], Response::HTTP_OK);
        } else {
            return View::create(['error' => 'Currency exchange rate not found!'], Response::HTTP_NOT_FOUND);
        }
    }
}
