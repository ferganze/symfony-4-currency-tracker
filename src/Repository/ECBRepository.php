<?php

namespace App\Repository;

use App\Entity\ECB;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ECB|null find($id, $lockMode = null, $lockVersion = null)
 * @method ECB|null findOneBy(array $criteria, array $orderBy = null)
 * @method ECB[]    findAll()
 * @method ECB[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ECBRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ECB::class);
    }

    // /**
    //  * @return ECB[] Returns an array of ECB objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ECB
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
