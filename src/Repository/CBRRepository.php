<?php

namespace App\Repository;

use App\Entity\CBR;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CBR|null find($id, $lockMode = null, $lockVersion = null)
 * @method CBR|null findOneBy(array $criteria, array $orderBy = null)
 * @method CBR[]    findAll()
 * @method CBR[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CBRRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CBR::class);
    }

    // /**
    //  * @return CBR[] Returns an array of CBR objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CBR
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
