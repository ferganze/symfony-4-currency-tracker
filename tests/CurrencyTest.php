<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\Client;

class CurrencyTest extends WebTestCase
{
    public function test()
    {
        $client = new Client();

        $request = $client->request('POST', getenv('APP_URL') . '/api/calculate/USD-GBP/100');

        $this->assertEquals(200, $request->getStatusCode());
    }
}